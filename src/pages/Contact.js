import React from "react";

const Contact = () => {
  return (
    <section id="contact" className="center">
      <div className="container">
        <p className="text-large center-align">Tertarik? Hubungi Kami!</p>
        <div className="row">
          <div className="center-align">
            <h1>Alamat</h1>
            <p>
              Jl. Arya Banjar Getas, Komplek Pertokoan Ampenan, Mataram, NTB
            </p>
          </div>
          <div className="center-align">
            <h1>Telepon</h1>
            <p>(+62) 89-012-341-231</p>
          </div>
          <div className="center-align">
            <h1>Email</h1>
            <p>support@perusahaanbaru.com</p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
