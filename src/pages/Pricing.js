import React from "react";

const Pricing = () => {
  return (
    <section id="pricing" className="center min-100">
      <div className="container">
        <p className="text-large center-align">
          Paket kami untuk memenuhi kebutuhan anda
        </p>
        <div className="row">
          <div className="center-align card primary">
            <h1>Basic</h1>
            <p className="text-small">
              *Harga mulai <strong>Rp. 100.000 / Bulan</strong>
            </p>
            <ul className="left-align">
              <li>Mencatat Barang Masuk</li>
              <li>Mencatat Barang Keluar</li>
              <li>Mencatat Hasil Keuntungan</li>
            </ul>
          </div>
          <div className="center-align card primary">
            <h1>Business</h1>
            <p className="text-small">
              *Harga mulai <strong>Rp. 199.000 / Bulan</strong>
            </p>
            <ul className="left-align">
              <li>Mencatat Barang Masuk</li>
              <li>Mencatat Barang Keluar</li>
              <li>Mencatat Hasil Keuntungan</li>
              <li>Support 7x24 jam</li>
            </ul>
          </div>
          <div className="center-align card primary">
            <h1>Enterpreneur</h1>
            <p className="text-small">
              *Harga mulai <strong>Rp. 316.000 / Bulan</strong>
            </p>
            <ul className="left-align">
              <li>Mencatat Barang Masuk</li>
              <li>Mencatat Barang Keluar</li>
              <li>Mencatat Hasil Keuntungan</li>
              <li>Support 7x24 jam</li>
              <li>Export data ke Excel</li>
              <li>AI Prediksi Penghasilan</li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Pricing;
