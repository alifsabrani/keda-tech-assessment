import React from "react";

const About = () => {
  return (
    <section id="about" className="center min-100">
      <div className="container">
        <p className="text-large center-align">Apa yang kami tawarkan?</p>
        <div className="row">
          <div className="center-align">
            <div className="icon center-align">
              <i className="fa fa-shopping-cart very-large"></i>
            </div>
            <p>Kemudahan transaksi</p>
          </div>
          <div className="center-align">
            <div className="icon center-align">
              <i className="fa fa-dollar very-large"></i>
            </div>
            <p>Harga terjangkau</p>
          </div>
          <div className="center-align">
            <div className="icon center-align">
              <i className="fa fa-info-circle very-large"></i>
            </div>
            <p>Dukungan jangka panjang</p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
