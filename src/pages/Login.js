import React from "react";

const Login = ({ closeModal }) => {
  return (
    <div className="modal">
      <div className="card">
        <div className="close-button">
          <button className="link" onClick={closeModal}>
            <i className="fa fa-times"></i>
          </button>
        </div>
        <p className="text-large center-align">Masuk</p>
        <div className="row">
          <form>
            <input name="username" placeholder="Username"></input>
            <input name="password" placeholder="Password"></input>
            <button className="primary normal">Login</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
