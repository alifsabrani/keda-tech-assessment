import React from "react";

const Home = () => {
  return (
    <section id="home" className="center v-center">
      <div>
        <p className="text-large center-align">
          Kami ada untuk membantu anda mengembangkan bisnis
        </p>
        <p className="center-align">
          <button className="primary">
            <span>
              <i className="fa fa-archive"></i>
            </span>
            Lihat Paket
          </button>
        </p>
      </div>
    </section>
  );
};

export default Home;
