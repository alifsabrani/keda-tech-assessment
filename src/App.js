import React, { useState } from "react";
import Header from "./components/Header";
import Home from "./pages/Home";
import About from "./pages/About";
import Pricing from "./pages/Pricing";
import Contact from "./pages/Contact";
import Login from "./pages/Login";

function App() {
  const [showModal, setShowModal] = useState(false);
  return (
    <div className="App">
      {showModal && <Login closeModal={() => setShowModal(false)}></Login>}
      <Header setShowModal={setShowModal}></Header>
      <Home></Home>
      <About></About>
      <Pricing></Pricing>
      <Contact></Contact>
    </div>
  );
}

export default App;
