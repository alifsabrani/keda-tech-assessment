import React from "react";
import NavItem from "./NavItem";
import logo from "../../logo.png";

const Header = ({ setShowModal }) => {
  const menu = [
    {
      to: "#about",
      name: "About",
    },
    {
      to: "#pricing",
      name: "Pricing",
    },
    {
      to: "#contact",
      name: "Contact",
    },
  ];
  return (
    <nav>
      <NavItem to="#home" type="link with-logo">
        <div className="logo">
          <img alt="logo" src={logo}></img>
        </div>
        <div>Home</div>
      </NavItem>
      <div>
        {menu.map((item) => (
          <NavItem to={item.to} type="link">
            {item.name}
          </NavItem>
        ))}
        <button className="primary outline" onClick={() => setShowModal(true)}>
          Login
        </button>
      </div>
    </nav>
  );
};

export default Header;
