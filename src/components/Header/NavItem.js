import React from "react";
import "../../styles/button.scss";

const NavItem = ({ to, type, children }) => {
  return (
    <a href={to}>
      <button className={type}>{children}</button>
    </a>
  );
};

export default NavItem;
